import { useEffect, useState } from "react";
import axios from "axios";

function getData(url, dataProcessor) {
  return axios
    .get(url)
    .then((results) => {
      const data = dataProcessor ? dataProcessor(results.data) : results.data;
      return {
        success: true,
        message: "",
        data,
      };
    })
    .catch((error) => {
      const state = { error: true, data: [] };
      if (error.response) {
        state.statusCode = error.response.statusCode;
        state.message = error.response.data.message || error.message;
      } else {
        state.message = error.message;
      }
      return state;
    });
}
function getDefaultState() {
  return {
    loading: true,
    error: false,
    message: "",
    success: false,
    data: null,
  };
}

function useGetData(url, dataProcessor) {
  const [apiState, setApiState] = useState(getDefaultState());

  useEffect(() => {
    getData(url, dataProcessor).then(setApiState);
  }, [url, dataProcessor]);

  return {
    ...apiState,

    refetch: (url, dataProcessor) => {
      getData(url, dataProcessor).then(setApiState);
    },
  };
}

export default useGetData;
