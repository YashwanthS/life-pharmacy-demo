import React from "react";

function Footer() {
  return (
    <div className="bg-[#000] flex justify-between items-center text-white px-32 h-64  py-3">
      <div className=" h-full  ">
        <div className=" flex items-center text-white">
          <img src="/images/logo.svg" alt="logo" />
          <p className=" text-[24px] font-semibold">Site Name</p>
        </div>
        <p className=" mt-3 text-sm font-thin">
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry.
        </p>
        <div className=" mt-5 w-full flex gap-3 items-center">
          <img src="/icons/fb.svg" alt="Facebook" />
          <img src="/icons/twitter.svg" alt="Google" />
          <img src="/icons/youtube.svg" alt="Google" />
          <img src="/icons/telegram.svg" alt="Google" />
        </div>
      </div>
      <div className=" h-full  ">
        <div className=" text-[24px] text-white font-semibold mb-3">
          Customer Support
        </div>
        <ul className=" text-xs flex flex-col gap-3">
          <li>Returns & Warranty</li>
          <li>Payments</li> <li>Shipping</li> <li>Terms and Services</li>
          <li>Privacy Policy</li>
        </ul>
      </div>

      <div className=" h-full  ">
        <div className=" text-[24px] text-white font-semibold mb-3">
          Corporate Info
        </div>
        <ul className=" text-xs flex flex-col gap-3">
          <li>About Us</li>
          <li>Brands</li> <li>Investors</li> <li>Cookies</li>
        </ul>
      </div>
      <div className=" h-full  ">
        <div className=" text-[24px] text-white font-semibold mb-3">
          Gift Card
        </div>
        <ul className=" text-xs flex flex-col gap-3">
          <li>Buy Gift Cards</li>
          <li>Redeem Card</li>
        </ul>
      </div>
      <div className=" h-full  ">
        <div className=" text-[24px] text-white font-semibold mb-3">
          Location
        </div>
        <ul className=" text-xs flex flex-col gap-3">
          <li>United State</li>
        </ul>
      </div>
    </div>
  );
}

export default Footer;
