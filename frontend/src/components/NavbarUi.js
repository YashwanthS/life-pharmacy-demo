import React from "react";
const navList = [
  {
    name: "Best Seller",
  },
  {
    name: "Products",
  },
  {
    name: "Brand",
  },
  {
    name: "Outlet",
  },
  {
    name: "Outlet",
  },
  {
    name: "Offers",
  },
  {
    name: "Our stories",
  },
];

function NavbarUi() {
  return (
    <div className="  flex  items-center gap-5 text-lg font-semibold pl-32  h-20 bg-gradient-to-b from-gray-200 to-white">
      {navList.map((nav) => {
        return <div>{nav.name}</div>;
      })}
    </div>
  );
}

export default NavbarUi;
