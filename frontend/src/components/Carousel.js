import React from "react";

function Carousel() {
  return (
    <div className=" w-full">
      <div className="carousel z-10   flex items-center min-h-screen m-2 px-32 pt-10  rounded-md ">
        <div className=" flex flex-col justify-between ">
          <h2 className=" uppercase leading-relaxed text-[86px] font-bold ">
            Workout Essential
            <br />
            supplements
          </h2>
          <p className=" text-[18] font-thin  ">
            In publishing and graphic design, Lorem ipsum is a placeholder text
            commonly used to demonstrate the visual form of a document or a
            typeface.
          </p>
          <button className=" mt-16 w-48 h-11 rounded-md shadow-xl   text-white bg-gradient-to-tl from-[#65C2Ed] to-[#F1ADFF]">
            Buy Now
          </button>
        </div>
        <div className=" flex justify-center items-center flex-col">
          <img src="/images/dashboard.png" alt="prduct" />
          <img
            src="/images/dashboar-shadow.png"
            alt="carousel"
            className="   relative top-1 "
          />
        </div>
      </div>

      <img
        src="/images/carousel-bg.png"
        alt="carousel"
        className=" relative bottom-[100px] "
      />

      <div className="flex justify-center max-w-full">
        <img
          src="/images/paginattion.png"
          alt="paginattion"
          className=" relative bottom-12"
        />
      </div>
    </div>
  );
}

export default Carousel;
