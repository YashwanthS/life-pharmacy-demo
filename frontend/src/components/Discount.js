import React from "react";

function Discount() {
  return (
    <div className="bg-[#FFF7EC] px-32 flex justify-between items-center py-10">
      <div>
        <h3 className="text-[42px] font-semibold">
          Grab an extra 5%
          <br />
          discount
        </h3>
        <p className=" text-[16px] text-gray-700 font-thin">
          Subscribe to monthly newsletter. Get the latest product updates and
          special offers delivered right to your inbox.{" "}
        </p>
      </div>
      <input
        type="email"
        name="email"
        placeholder="Enter email address"
        className="border-gradient w-96 px-3 border-gradient-purple h-11 bg-[#FFF7EC]"
      />
    </div>
  );
}

export default Discount;
