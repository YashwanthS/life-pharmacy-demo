import React from "react";
import useGetData from "../lib/useGetData";

const bestSellers = [
  {
    productName: "MB Fuel Whey Protein Immunity +2kg ",
    mrp: "",
  },
];

function BestSellers() {
  const bestSellerData = useGetData(`https://fakestoreapi.com/products`);

  const data = bestSellerData.data || [];
  console.log("BestSeller data>>", data || []);

  return (
    <div className=" grid grid-cols-2 md:grid-cols-4 gap-5">
      {data.map((sellerData) => {
        return (
          <div
            key={sellerData.id}
            className=" flex flex-col gap-2 bg-white w-64 rounded-md p-3 text-sm"
          >
            <img src={sellerData.image} alt="image" className="h-44"/>
            <h3 className=" font-medium">{sellerData.title}</h3>
            <div className=" text-gray-300 line-through">MRP $5269</div>
            <div className=" flex items-center gap-2">
              <span className=" font-semibold">Price: ${sellerData.price}</span>
              <span className=" text-xs text-[#4E966E]">23 % off</span>
            </div>
            <button className=" border-gradient border-gradient-purple gap-3 bg-white w-44 flex items-center justify-center h-12 ">
              <img src="/images/cart.svg" alt="cart" />
              <span className=" text-[16px] font-semibold">Add to Cart</span>
            </button>
          </div>
        );
      })}
    </div>
  );
}

export default BestSellers;
