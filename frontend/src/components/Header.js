import React from "react";

function Header() {
  return (
    <div className="  flex justify-between  items-center gap-5 text-lg font-semibold px-32  h-20 bg-white">
      <img src="/images/logo.svg" alt="logo" />

      <div className=" flex items-center divide-x gap-3 divide-gray-200">
        <div className=" flex gap-2 items-center p-1 border border-gray-400 rounded-md h-11 w-80 ">
          <img src="/icons/search.svg" alt="search" className=" w-5 h-5" />
          <input
            type="text"
            name="search"
            placeholder="Search for Product"
            className=" text-xs text-gray-500 focus:outline-none"
          />
        </div>
        <div className=" pl-3 gap-3 flex items-center justify-between">
          <img src="/icons/fav.svg" alt="fav" className="w-4 h-4" />
          <img src="/images/cart.svg" alt="cart" className="w-4 h-4" />
          <button className="  w-48 h-11 rounded-md shadow-xl flex justify-center items-center gap-3   text-white bg-gradient-to-tl from-[#65C2Ed] to-[#F1ADFF]">
            <span>Get Start</span>
            <img src="/icons/arrow.svg" alt="arrrow" />
          </button>
        </div>
      </div>
    </div>
  );
}

export default Header;
