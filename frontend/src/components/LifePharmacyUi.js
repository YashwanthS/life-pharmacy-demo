import React from "react";
import NavbarUi from "./NavbarUi";
import Carousel from "./Carousel";
import Categories from "./Categories";
import Discount from "./Discount";
import Footer from "./Footer";
import Header from "./Header";
import Products from "./Produts";

function LifePharmacyUi() {
  return (
    <div className=" w-full">
      <Header />
      <NavbarUi />
      <Carousel />
      <Categories />
      <Products />
      <Discount />
      <Footer />
    </div>
  );
}

export default LifePharmacyUi;
