import React, { useState } from "react";
import BestSellers from "./BestSellers";

function Products() {
  const [openTab, setOpenTab] = useState(1);

  return (
    <div className=" bg-gradient-to-br from-[#f7f0f9] to-[#daf2fb]  min-h-screen px-32 py-10 ">
      <nav
        aria-label="Tabs"
        className="  bg-white    h-12  items-center w-64 flex "
      >
        <Tab openTab={openTab} setOpenTab={setOpenTab} tabId={1}>
          Best Sellers
        </Tab>
        <Tab openTab={openTab} setOpenTab={setOpenTab} tabId={2}>
          Category's
        </Tab>
      </nav>
      <div className=" mt-5">
        <TabDetails openTab={openTab} tabId={1}>
          <BestSellers />
        </TabDetails>

        <TabDetails openTab={openTab} tabId={2}>
          <BestSellers />
        </TabDetails>
      </div>
    </div>
  );
}

export default Products;

function Tab({ openTab, setOpenTab, tabId, children }) {
  return (
    <button
      onClick={(e) => {
        e.preventDefault();
        setOpenTab(tabId);
      }}
      className={` flex items-center whitespace-nowrap justify-center gap-2 focus:outline-none font-medium  md:text-base text-ving-black text-xs  tracking-wider p-3 h-12 w-full ${
        openTab === tabId
          ? " bg-black text-white rounded-md"
          : " border border-gray-300 text-gray-300 rounded-md "
      } `}
    >
      {children}
    </button>
  );
}

function TabDetails({ openTab, tabId, children }) {
  return tabId === openTab ? children : null;
}
