import React from "react";
import useGetData from "../lib/useGetData";

function getBgcolor(data) {
  let bg = "";
  if (data === "electronics") {
    bg = "bg-[#EBF8E6]";
  } else if (data === "jewelery") {
    bg = "bg-[#FFE8ED]";
  } else if (data === "men's clothing") {
    bg = "bg-[#E5EDFA]";
  } else if (data === "women's clothing") {
    bg = "bg-[#FFEFB6]";
  }

  return bg;
}

const categories = [
  {
    title: "Vitamins",
    img: "/images/vitamins.svg",
    alt: "Vitamins",
    bg: "bg-[#EBF8E6] ",
  },
  {
    title: "Medicine",
    img: "/images/medicing.svg",
    alt: "Medicine",
    bg: "bg-[#FFE8ED] ",
  },
  {
    title: "Home Healthcare",
    img: "/images/healthcare.svg",
    alt: "Home Healthcare",
    bg: "bg-[#E5EDFA]",
  },
  {
    title: "Fish Oil & Omega",
    img: "/images/fish-oil.svg",
    alt: "Fish Oil & Omega",
    bg: "bg-[#FFEFB6]",
  },
];

function Categories() {
  const categories = useGetData(`https://fakestoreapi.com/products/categories`);

  const data = categories.data || [];
  console.log("categories data>>", data || []);
  return (
    <div className="  mb-20 px-32 pt-10  rounded-md ">
      <div className=" flex justify-between items-center">
        <h3 className=" font-semibold text-[24px]">Top Category's</h3>
        <p className=" text-[16px] font-semibold text-[#100F1A]">View All</p>
      </div>

      <div className=" grid md:grid-cols-4 gap-5 mt-3 grid-cols-1  items-center">
        {data.map((category, index) => {
          const bgColor = getBgcolor(data[index]);

          return (
            <div
              key={index}
              className={`flex px-3 ${bgColor} justify-between items-center  h-32 rounded-md `}
            >
              <h3 className=" text-base font-semibold text-[#393846]">
                {data[index]}
              </h3>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default Categories;
