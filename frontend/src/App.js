import LifePharmacyUi from "./components/LifePharmacyUi";

export default function App() {
  return (
   <div className=" w-full">
   <LifePharmacyUi />
   </div>
  );
}
